import React from 'react';
import { Text } from 'react-native';

import { SafeAreaContainer, CenteredView, Button } from '../common-components';

import { NavigationProps } from '@navigation';

export default function Second({ navigation }: NavigationProps<'Second'>) {
    return (
        <SafeAreaContainer>
            <CenteredView>
                <Text>Second</Text>
                <Button
                    containerStyle={{ marginTop: 30 }}
                    title={'Go Back'}
                    onPress={() => navigation.goBack()}
                />
            </CenteredView>
        </SafeAreaContainer>
    )
}