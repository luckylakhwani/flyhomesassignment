import React from 'react';
import { Text, StyleSheet } from 'react-native';

import { SafeAreaContainer, CenteredView, Button, Card } from '../common-components';

import { NavigationProps } from '@navigation';
import { fonts } from '../theme';

export default function First({ navigation }: NavigationProps<'First'>) {
    return (
        <SafeAreaContainer>
            <CenteredView>
                <Card>
                    <Text style={styles.textStyle}>First</Text>
                </Card>
                <Button
                    title={'Navigate'}
                    onPress={() => navigation.navigate('Second')}
                />
            </CenteredView>
        </SafeAreaContainer>
    )
}

const styles = StyleSheet.create({
    textStyle: {
        fontFamily: fonts.fontFamily.Regular,
        fontSize: fonts.fontSize.Large,
    }
})