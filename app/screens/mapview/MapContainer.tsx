import React, { useEffect, useRef } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import MapView from 'react-native-maps';
import { SafeAreaContainer } from '../../common-components';
import BottomSheet from '../../common-components/BottomSheet';
import Logger from '../../utils/Logger';
import Marker from './Marker';

import { Listing, MapData } from './types';
import { useMapData } from './useMapData';

export default function MapContainer() {
    const { mapData, error, isLoading, getMapData } = useMapData();
    const mapRef = useRef<MapView>(null);

    useEffect(() => {
        getMapData();
    }, [])

    function loadMapData() {
        if (mapData) {

            animateToCurrentMarker();

            return mapData.listings.map((marker: Listing, index: number) => (
                <Marker
                    key={index}
                    coordinate={{ latitude: marker.lt, longitude: marker.ln }}
                    title={marker.city}
                    description={marker.addr}
                />))
        }
        return null
    }

    function animateToCurrentMarker() {
        const firstMarker = (mapData as MapData).listings[0]
        const currentRegion = {
            latitude: firstMarker.lt,
            longitude: firstMarker.ln,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
        };
        mapRef?.current?.animateToRegion(currentRegion, 3 * 1000);
    }

    if (error) {
        Logger.log("MapData error", error);
    }

    return (
        <SafeAreaContainer>
            {isLoading && <ActivityIndicator />}
            {mapData &&
                <MapView
                    style={{
                        ...StyleSheet.absoluteFillObject,
                    }}
                    ref={mapRef}>
                    {loadMapData()}
                </MapView>
            }
            <BottomSheet />
        </SafeAreaContainer>
    )
}