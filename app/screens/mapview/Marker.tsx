import React from 'react';
import { Marker as RNMarker, MarkerProps as RNMarkerProps } from 'react-native-maps';

interface MarkerProps extends RNMarkerProps {
}

export default function Marker(props: MarkerProps) {
    return (
        <RNMarker
            coordinate={props.coordinate}
            title={props.title}
            description={props.description}
        />
    )
}