export interface MapData {
    listings: [Listing],
    count: number
}

export interface Listing {
    lt: number;
    ln: number;
    id: number;
    p: number;
    addr: string;
    city: string;
}