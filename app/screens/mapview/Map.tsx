import React from 'react';
import { StyleSheet } from 'react-native';
import MapView, { MapViewProps as RNMapViewProps } from 'react-native-maps';

interface MapViewProps extends RNMapViewProps {
    children: React.ReactNode;
    ref: any;
}

export default function Map(props: MapViewProps) {
    return (
        <MapView
            ref={props.ref}
            style={{
                ...StyleSheet.absoluteFillObject,
            }}
            initialRegion={{
                latitude: 37.876774,
                longitude: -123.957482,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }}
        >{props.children}
        </MapView>
    )
}
