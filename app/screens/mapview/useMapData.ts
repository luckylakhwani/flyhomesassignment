import { useReducer } from 'react';
import { getAPIURL } from '../../api-config';
import { useHttpGet } from '../../hooks';

import { MapData } from './types';

const actions = {
    GET_MAPDATA: "GET_MAPDATA",
    IS_LOADING: "IS_LOADING",
    IS_ERROR: "IS_ERROR",
}

function reducer(state: any, action: any) {
    if (action) {
        switch (action.type) {
            case actions.GET_MAPDATA:
                return {
                    ...state,
                    mapData: action.payload
                }

            case actions.IS_LOADING:
                return {
                    ...state,
                    isLoading: action.payload
                }

            case actions.IS_ERROR:
                return {
                    ...state,
                    error: action.payload
                }
        }
    }
}

export function useMapData() {
    let [data, dispatch] = useReducer(reducer, {
        error: null,
        isLoading: false,
        mapData: null,
    });

    async function getMapData() {
        try {
            dispatch({ type: actions.IS_LOADING, payload: true })

            const res = await useHttpGet<MapData>(getAPIURL(''));
            dispatch({ type: actions.GET_MAPDATA, payload: res })
        }
        catch (err) {
            dispatch({ type: actions.IS_ERROR, payload: err });
        }
        finally {
            dispatch({ type: actions.IS_LOADING, payload: false });
        }
    }

    return {
        ...data,
        getMapData: () => getMapData()
    }
}