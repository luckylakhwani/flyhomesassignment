export const Endpoints = {
}

const API_URL = 'https://gist.githubusercontent.com/vivekverma1993/29379139e49f61c55d0e28a888ccaa0d/raw/6dbf62c815e1890919ddb3a904e202add6bd5fb7/test.json'

export function getAPIURL(Endpoint: string) {
    return API_URL + Endpoint;
}