import React from "react";
import { View, StyleSheet } from 'react-native';

import { colors } from "../theme";

interface CardProps {
    children?: React.ReactNode
}

export default function Card({ children }: CardProps) {
    return (
        <View style={styles.container}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.offWhite,
        padding: 15,
        borderRadius: 8,
        elevation: 3,
        shadowOffset: { width: 1, height: 1 },
        shadowColor: colors.gray,
        shadowOpacity: 0.3,
        width: '80%',
        margin: 10,
        justifyContent: 'center'
    }
})