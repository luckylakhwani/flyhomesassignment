import React from 'react';
import { StyleSheet, View } from 'react-native';

import { colors } from '../theme';

function Separator() {
    return (
        <View style={styles.separator} />
    )
}

export default Separator;

const styles = StyleSheet.create({
    separator: {
        alignSelf: 'center',
        width: '95%',
        height: 1,
        backgroundColor: colors.primaryDark
    }
})