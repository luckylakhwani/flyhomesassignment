import React, { useEffect } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';
import Animated, { useAnimatedStyle, useSharedValue, withTiming } from 'react-native-reanimated';

const { height: SCREEN_HEIGHT } = Dimensions.get('window')
const MAX_HEIGHT = SCREEN_HEIGHT - 100

export default function BottomSheet() {
    const translateY = useSharedValue(0)

    const context = useSharedValue({ y: 0 })

    const panGesture = Gesture.Pan()
        .onStart(() => {
            context.value = { y: translateY.value }
        })
        .onUpdate(event => {
            translateY.value = event.translationY + context.value.y
            translateY.value = Math.max(translateY.value, -MAX_HEIGHT)
        });

    const animatedBottomSheetStyle = useAnimatedStyle(() => {
        return {
            transform: [{ translateY: translateY.value }]
        }
    })

    useEffect(() => {
        translateY.value = withTiming(-SCREEN_HEIGHT / 3)
    }, [])

    return (
        <GestureDetector gesture={panGesture}>
            <Animated.View style={[styles.container, animatedBottomSheetStyle]}>
                <View style={styles.line} />
            </Animated.View>
        </GestureDetector>
    )
}

const styles = StyleSheet.create({
    container: {
        height: SCREEN_HEIGHT,
        width: '100%',
        backgroundColor: 'gray',
        position: 'absolute',
        top: SCREEN_HEIGHT,
        borderRadius: 15
    },
    line: {
        width: 50,
        height: 3,
        backgroundColor: 'white',
        alignSelf: 'center',
        marginVertical: 12,
        borderRadius: 5
    }
})