import React from "react";
import { SafeAreaView, StyleSheet, ViewProps } from "react-native";

interface SafeAreaProps extends ViewProps {
    children?: React.ReactNode;
}

function SafeAreaContainer(props: SafeAreaProps) {
    return (
        <SafeAreaView style={[styles.containerStyle, props.style]}>
            {props.children}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default SafeAreaContainer;