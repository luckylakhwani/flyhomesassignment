import React from "react";
import { View, StyleSheet, ViewProps } from "react-native";

interface CVProps extends ViewProps {
    children?: React.ReactNode;
}

function CenteredView(props: CVProps) {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default CenteredView;