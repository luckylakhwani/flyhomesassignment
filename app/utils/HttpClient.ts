import Logger from "./Logger";

export default interface HttpClientRequestParameters {
    apiUrl: string;
}

import { axiosInstance } from '../api-config';

export class HttpClient {
    get<T>(parameters: HttpClientRequestParameters): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            const { apiUrl } = parameters;
            Logger.log('URL' + apiUrl);
            axiosInstance
                .get(apiUrl)
                .then((response: any) => {
                    Logger.log(
                        "Success in Get: ",
                        response.data as T
                    );
                    if (response && response.status === 200) {
                        resolve(response.data as T);
                    } else {
                        reject(response);
                    }
                })
                .catch((response: any) => {
                    Logger.log("Failure in Get: ", response);
                    reject(response);
                });
        });
    }
}
