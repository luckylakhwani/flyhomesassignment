import { Alert } from "react-native";

export const showAlert = (title: string, message: string, buttonTitle?: string, callback?: () => void) => {
    Alert.alert(title, message, [
        {
            text: buttonTitle || "OK",
            onPress: callback
        }
    ])
}