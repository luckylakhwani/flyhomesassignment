import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export type NavigationParamList = {
    //List of screens to navigate
    First: undefined,
    Second: undefined,
    Map: undefined,
};

export type NavigationProps<T extends keyof NavigationParamList> = {
    navigation: NativeStackNavigationProp<NavigationParamList, T>;
    route: RouteProp<NavigationParamList, T>;
};