import React from 'react';

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationParamList } from "./NavigationParamList";

export const Stack = createNativeStackNavigator<NavigationParamList>();

export function addScreen(name: keyof NavigationParamList, component: any, title: string, showBackHeader: boolean) {
    return (<Stack.Screen
        name={name}
        component={component}
        options={{
            title: title,
            headerBackButtonMenuEnabled: showBackHeader
        }}
    />)
}
