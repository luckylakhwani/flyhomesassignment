import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import SampleStack from './SampleStack';

function RootNavigator() {
    return (
        <NavigationContainer>
            <SampleStack />
        </NavigationContainer>
    )
}

export default RootNavigator;