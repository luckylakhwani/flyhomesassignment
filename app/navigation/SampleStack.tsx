import React from 'react';

import { Stack, addScreen } from './ConfgureStack';
import { First, Second } from '../screens';

import MapContainer from '../screens/mapview/MapContainer';

export default function SampleStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>
            {addScreen("Map", MapContainer, "", false)}
        </Stack.Navigator>
    )
}