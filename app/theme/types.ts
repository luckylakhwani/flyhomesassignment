import { Fonts, FontSize } from "../theme";

export interface AppFont {
    fontFamily: typeof Fonts;
    fontSize: typeof FontSize;
}

export interface AppTheme {
    colors: BaseColors;
    fonts: AppFont;
}

export interface BaseColors {
    primaryDark: string;
    black: string;
    white: string;
    red: string;
    textColor: string;
    transparent: string;
    gray: string;
    orange: string;
    offWhite: string;
}
