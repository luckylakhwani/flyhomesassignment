import { Fonts, FontSize } from "./FontHelper";
import { AppTheme, AppFont } from "./types";

const theme: AppTheme = getTheme();

function getTheme() {
    let colors = require('../assets/themes/theme.json');
    let fonts: AppFont = {
        fontFamily: Fonts,
        fontSize: FontSize
    }
    let theme = {
        colors: colors,
        fonts: fonts
    }
    return theme;
}

module.exports = {
    theme: theme
};

