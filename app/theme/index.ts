import { AppTheme } from './types';

export * from './FontHelper';
export * from './types';

const ThemeParser = require('./ThemeParser');
const appTheme = ThemeParser.theme as AppTheme;

const { colors, fonts } = appTheme;
export { colors, fonts };

