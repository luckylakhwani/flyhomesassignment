import React from 'react';
import { RootNavigator } from './app/navigation';

if (__DEV__) {
  import('./app/utils/ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

function App() {
  return (
    <RootNavigator />
  )
};

export default App;
